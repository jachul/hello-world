package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        String style = "<style type='text/css' media='screen'>";
        // color code can be #6db23e testing... 
        style += "body { background-color:  #6db23e; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-size: 250%; }";
        style += "</style>";
  


        String message = "Hello World KBLife 데모 0408 Gitlabber.!!!";
        String body = "<body>" + message + "</body>";

// TODO 
        return style + body;
    }

}
